import sympy
from scipy import optimize as opt
from cvxopt import matrix, solvers

#Symbolic Equation library
class SymEq:
    # Construct equation with sympy
    # rationalize it if flag is set to True
    # is_eq is True for DAI's and actions
    @staticmethod
    def construct_eqn(eqn, is_eq, rationalize):
        try:
            if is_eq:
                eqn_split = eqn.split('=')
                lhs, rhs = eqn_split[0], eqn_split[1]
                print (lhs, rhs)
                eqn = sympy.Eq(sympy.sympify(lhs), sympy.sympify(rhs))
            else:
                eqn = sympy.sympify(eqn)
                if type(eqn) is bool:
                    return eqn
        except SyntaxError:
            print("Invalid expression.")

        if rationalize:
            eqn = SymEq.rationalize(eqn)
        return eqn

    def to_str(self):
        return str(self.eqn)

    # Given an equation convert all decimals to integers by multiplying by LCM
    @staticmethod
    def rationalize(expr):
        if expr.is_Relational:
            mult = max(SymEq.get_factor(expr.lhs), SymEq.get_factor(expr.rhs))
            return expr.func(mult*expr.lhs, mult*expr.rhs)

    @staticmethod
    def get_factor(expr):
        exp = 0
        if expr.is_Add:
            terms = expr.args
        else:
            terms = [expr]

        for term in terms:
            if term.is_Number:
                exp = max(exp, SymEq.get_term_exp(term))
            for unit in term.args:
                if unit.is_Number:
                    exp = max(exp, SymEq.get_term_exp(unit))

        return 10**exp

    @staticmethod
    def get_term_exp(unit):
        return len(str(float(unit)))-str(float(unit)).index('.')-1

    # Return A, B matrices for expressions with x as varList
    @staticmethod
    def get_eqn_matrix(expressions, varList):
        exprs = []
        for expr in expressions.split('&&'):
            if '==' in expr:
                eq = expr.split('==')
                exprs.append(sympy.sympify(eq[0]+'<='+eq[1]))
                exprs.append(sympy.sympify(eq[0]+'>='+eq[1]))
            else:
                sym_expr = sympy.sympify(expr)
                if sym_expr.func==sympy.StrictLessThan:
                    sym_expr = sympy.LessThan(sym_expr.lhs, sym_expr.rhs)
                elif sym_expr.func==sympy.StrictGreaterThan:
                    sym_expr = sympy.GreaterThan(sym_expr.lhs, sym_expr.rhs)
                exprs.append(sym_expr)
        aMatrix=[[0 for v in varList] for expr in exprs]
        bMatrix=[0 for expr in exprs]
        eqMatrix=[]
        for i,expr in enumerate(exprs):
            eqMatrix.append([expr.rel_op])
            expr = expr.lhs-expr.rhs
            expr *= SymEq.get_factor(expr)
            for v in expr.free_symbols:
                aMatrix[i][varList.index(str(v))] = expr.as_coefficients_dict()[v]
                bMatrix[i] = [-expr.as_coefficients_dict()[sympy.numbers.One()]]
        return [aMatrix, bMatrix, eqMatrix]


    @staticmethod
    def check_boundedness(a_m, b_m, eq_m, var_list):
        A, B = [], []
        for r, a in enumerate(a_m):
            if eq_m[r][0] == '<=':
                A.append(a)
                B.append(b_m[r][0])
            else:
                A.append([-1 * x for x in a])
                B.append(-1 * b_m[r][0])

        # Solve minimum
        C = [0.0 for v in var_list]
        bounds = [(None, None) for v in var_list]
        for i, v in enumerate(var_list):
            C[i] = 1.0
            min = opt.linprog(C, A_ub=A, b_ub=B, bounds=bounds)
            if not min.success:
                return False

            C[i] = 0.0
        return True

    # Return whether the initial set is bounded or not
    @staticmethod
    def check_boundedness_using_cvxopt(a_m, b_m, eq_m, var_list):
        A, B = [], []
        for r, a in enumerate(a_m):
            if eq_m[r][0] == '<=':
                A.append(a)
                B.append(b_m[r][0])
            else:
                A.append([-1 * x for x in a])
                B.append(-1 * b_m[r][0])
        A=[[float(x) for x in row] for row in A];
        A=matrix(A).T
        B=[float(x) for x in B];
        B=matrix(B)
        # Solve minimum
        C = [0.0 for v in var_list]
        bounds = [(None, None) for v in var_list]
        for i, v in enumerate(var_list):
            C[i] = 1.0
            min = solvers.lp(matrix(C),A,B)
            if min['status'] != "optimal":
                return False

            C[i] = 0.0
        return True

    # Represent u**n as u*u*...u for the simulators.
    @staticmethod
    def convert_pow(expr):
        #SymEq.pow_to_mul(expr)
        return str(SymEq.convert_pow_helper(expr))

    @staticmethod
    def convert_pow_helper(expr):
        if not expr.args:
            return expr
        conv_args = [SymEq.convert_pow_helper(arg) for arg in expr.args]
        if expr.is_Pow and expr.exp>0:
            print("this expr is pow:", expr)
            print(expr.base, expr.exp)
            return sympy.Symbol('*'.join(['('+str(conv_args[0])+')']*int(conv_args[1])))
        return expr.func(*conv_args)

    @staticmethod
    def pow_to_mul(expr):
        """
        Convert integer powers in an expression to Muls, like a**2 => a*a.
        """
        pows = list(expr.atoms(sympy.Pow))
        if any(not e.is_Integer for b, e in (i.as_base_exp() for i in pows)):
            raise ValueError("A power contains a non-integer exponent")
        print(pows)
        for b,e in (i.as_base_exp() for i in pows):
            print(b,e)
        repl = zip(pows, (sympy.Mul(*[b]*e,evaluate=False) for b,e in (i.as_base_exp() for i in pows)))
        #print repl
        return expr.subs(repl)

    # Negate guard to construct invariant
    @staticmethod
    def construct_invariant(guard):
        inv_eqn = []
        for eqn in guard.expr:
            print('Eqn is: ' + str(eqn))
            if eqn.func==sympy.LessThan:
                inv = sympy.GreaterThan(*eqn.args)
            elif eqn.func==sympy.GreaterThan:
                inv = sympy.LessThan(*eqn.args)    
            elif eqn.func==sympy.StrictLessThan:
                inv = sympy.StrictGreaterThan(*eqn.args)    
            elif eqn.func==sympy.StrictGreaterThan:
                inv = sympy.StrictLessThan(*eqn.args)
            inv_eqn.append(inv)
        inv_eqn = [str(inv) for inv in inv_eqn]
        return Invariant('||'.join(inv_eqn))

    # Return all free vars in exprs
    @staticmethod
    def vars_used(exprs):
        var_set = set()
        for eqn in exprs:
            var_set = var_set.union(eqn.free_symbols)
        return [str(v) for v in var_set]

    # Return if expr is linear
    @staticmethod
    def is_linear(expr):
        syms = expr.free_symbols
        for x in syms:
            for y in syms:
                try:
                    if not sympy.Eq(sympy.diff(expr, x, y), 0):
                        return False
                except TypeError:
                    return False
        return True


if __name__ == "__main__":
    initial_set = "x1 >= 0.0060145 && x1 <= 0.0115005 && x2 >= -0.0009624 && x2 <= 0.0025689 && x3 >= 0.0177578 && x3 <= 0.0257479 && x4 >= -0.0047553 && x4 <= -0.0001524 && x5 >= 0.0177717 && x5 <= 0.0371327 && x6 >= -0.0122932 && x6 <= -0.0046144 && x7 >= -0.0026115 && x7 <= 0.0003953 && x8 >= -0.0122392 && x8 <= -0.0049088 && x9 >= 0.0014043 && x9 <= 0.0043716 && x10 >= -0.0219818 && x10 <= -0.0128509 && x11 >= -0.0396104 && x11 <= -0.0274146 && x12 >= -0.0042632 && x12 <= 0.0284950 && x13 >= -0.0070418 && x13 <= -0.0037184 && x14 >= -0.0093470 && x14 <= -0.0011582 && x15 >= 0.0022889 && x15 <= 0.0100333 && x16 >= -0.0055435 && x16 <= 0.0159219 && x17 >= -0.0104197 && x17 <= -0.0070923 && x18 >= 0.0177905 && x18 <= 0.0271285 && x19 >= 0.0422691 && x19 <= 0.0571911 && x20 >= -0.0215207 && x20 <= 0.0191948 && x21 >= -0.0175572 && x21 <= -0.0100114 && x22 >= -0.0297843 && x22 <= -0.0033456 && x23 >= 0.0177942 && x23 <= 0.0262628 && x24 >= 0.0209676 && x24 <= 0.0558912 && x25 >= -0.0515384 && x25 <= -0.0326774 && t==0"
    var_list = ['x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7', 'x8', 'x9', 'x10', 'x11', 'x12', 'x13', 'x14', 'x15', 'x16', 'x17', 'x18', 'x19', 'x20', 'x21', 'x22', 'x23', 'x24', 'x25', 't']
    a_m, b_m, eq_m = SymEq.get_eqn_matrix(initial_set, var_list)

    bounded = SymEq.check_boundedness_using_cvxopt(a_m, b_m, eq_m, var_list)
    assert(bounded == True)

    initial_set = "x1 >= 0.1060145 && x1 <= 0.0115005 && x2 >= -0.0009624 && x2 <= 0.0025689 && x3 >= 0.0177578 && x3 <= 0.0257479 && x4 >= -0.0047553 && x4 <= -0.0001524 && x5 >= 0.0177717 && x5 <= 0.0371327 && x6 >= -0.0122932 && x6 <= -0.0046144 && x7 >= -0.0026115 && x7 <= 0.0003953 && x8 >= -0.0122392 && x8 <= -0.0049088 && x9 >= 0.0014043 && x9 <= 0.0043716 && x10 >= -0.0219818 && x10 <= -0.0128509 && x11 >= -0.0396104 && x11 <= -0.0274146 && x12 >= -0.0042632 && x12 <= 0.0284950 && x13 >= -0.0070418 && x13 <= -0.0037184 && x14 >= -0.0093470 && x14 <= -0.0011582 && x15 >= 0.0022889 && x15 <= 0.0100333 && x16 >= -0.0055435 && x16 <= 0.0159219 && x17 >= -0.0104197 && x17 <= -0.0070923 && x18 >= 0.0177905 && x18 <= 0.0271285 && x19 >= 0.0422691 && x19 <= 0.0571911 && x20 >= -0.0215207 && x20 <= 0.0191948 && x21 >= -0.0175572 && x21 <= -0.0100114 && x22 >= -0.0297843 && x22 <= -0.0033456 && x23 >= 0.0177942 && x23 <= 0.0262628 && x24 >= 0.0209676 && x24 <= 0.0558912 && x25 >= -0.0515384 && x25 <= -0.0326774 && t==0"
    a_m, b_m, eq_m = SymEq.get_eqn_matrix(initial_set, var_list)

    bounded = SymEq.check_boundedness_using_cvxopt(a_m, b_m, eq_m, var_list)
    assert(bounded == False)

    initial_set = "x1==1.2&& x2==1.05&&x3==1.5&&x4==2.4&&x5==1.0&&x6==0.1&&x7==0.45"
    var_list = ['x1', 'x2', 'x3', 'x4', 'x5', 'x6', 'x7']
    a_m, b_m, eq_m = SymEq.get_eqn_matrix(initial_set, var_list)
    bounded = SymEq.check_boundedness_using_cvxopt(a_m, b_m, eq_m, var_list)
    assert(bounded == True)

    initial_set = "x1>=1.2"
    var_list = ['x1']
    a_m, b_m, eq_m = SymEq.get_eqn_matrix(initial_set, var_list)
    bounded = SymEq.check_boundedness_using_cvxopt(a_m, b_m, eq_m, var_list)
    assert(bounded == True)

    initial_set = "x1<=1.2"
    var_list = ['x1']
    a_m, b_m, eq_m = SymEq.get_eqn_matrix(initial_set, var_list)
    bounded = SymEq.check_boundedness_using_cvxopt(a_m, b_m, eq_m, var_list)
    assert(bounded == False)

    initial_set = "u>=0&&v>=0&&stim_local>=1&&t==0"
    var_list = ['u','v','stim_local','t']
    a_m, b_m, eq_m = SymEq.get_eqn_matrix(initial_set, var_list)
    bounded = SymEq.check_boundedness_using_cvxopt(a_m, b_m, eq_m, var_list)
    assert(bounded == True)

    initial_set = "u>=0&&v<=0&&stim_local>=1&&t==0"
    var_list = ['u','v','stim_local','t']
    a_m, b_m, eq_m = SymEq.get_eqn_matrix(initial_set, var_list)
    bounded = SymEq.check_boundedness_using_cvxopt(a_m, b_m, eq_m, var_list)
    assert(bounded == False)