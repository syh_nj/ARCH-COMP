function Hf=hessianTensor_cstrContRef(x,u,p)



 Hf{1} = sparse(6,6);

Hf{1}(2,1) = -(630000000000000*exp(-8750/(x(2) + 350)))/(x(2) + 350)^2;
Hf{1}(1,2) = -(630000000000000*exp(-8750/(x(2) + 350)))/(x(2) + 350)^2;
Hf{1}(2,2) = (1260000000000000*exp(-8750/(x(2) + 350))*(x(1) + 1/2))/(x(2) + 350)^3 - (5512500000000000000*exp(-8750/(x(2) + 350))*(x(1) + 1/2))/(x(2) + 350)^4;


 Hf{2} = sparse(6,6);

Hf{2}(2,1) = (33740585774058576875*exp(-8750/(x(2) + 350)))/(256*(x(2) + 350)^2);
Hf{2}(1,2) = (33740585774058576875*exp(-8750/(x(2) + 350)))/(256*(x(2) + 350)^2);
Hf{2}(2,2) = (147615062761506273828125*exp(-8750/(x(2) + 350))*(x(1) + 1/2))/(128*(x(2) + 350)^4) - (33740585774058576875*exp(-8750/(x(2) + 350))*(x(1) + 1/2))/(128*(x(2) + 350)^3);


 Hf{3} = sparse(6,6);

Hf{3}(4,3) = -(630000000000000*exp(-8750/(x(4) + 350)))/(x(4) + 350)^2;
Hf{3}(3,4) = -(630000000000000*exp(-8750/(x(4) + 350)))/(x(4) + 350)^2;
Hf{3}(4,4) = (1260000000000000*exp(-8750/(x(4) + 350))*(x(3) + 1/2))/(x(4) + 350)^3 - (5512500000000000000*exp(-8750/(x(4) + 350))*(x(3) + 1/2))/(x(4) + 350)^4;


 Hf{4} = sparse(6,6);

Hf{4}(4,3) = (33740585774058576875*exp(-8750/(x(4) + 350)))/(256*(x(4) + 350)^2);
Hf{4}(3,4) = (33740585774058576875*exp(-8750/(x(4) + 350)))/(256*(x(4) + 350)^2);
Hf{4}(4,4) = (147615062761506273828125*exp(-8750/(x(4) + 350))*(x(3) + 1/2))/(128*(x(4) + 350)^4) - (33740585774058576875*exp(-8750/(x(4) + 350))*(x(3) + 1/2))/(128*(x(4) + 350)^3);
