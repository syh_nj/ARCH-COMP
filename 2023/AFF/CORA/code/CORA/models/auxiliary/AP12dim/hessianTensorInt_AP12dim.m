function Hf=hessianTensorInt_AP12dim(x,u)



 Hf{1} = interval(sparse(13,13),sparse(13,13));

Hf{1}(6,2) = x(9);
Hf{1}(9,2) = x(6);
Hf{1}(2,6) = x(9);
Hf{1}(9,6) = x(2);
Hf{1}(2,9) = x(6);
Hf{1}(6,9) = x(2);


 Hf{2} = interval(sparse(13,13),sparse(13,13));

Hf{2}(10,7) = x(11);
Hf{2}(11,7) = x(10);
Hf{2}(7,10) = x(11);
Hf{2}(11,10) = x(7);
Hf{2}(7,11) = x(10);
Hf{2}(10,11) = x(7);


 Hf{3} = interval(sparse(13,13),sparse(13,13));

Hf{3}(3,3) = 6*x(3);


 Hf{4} = interval(sparse(13,13),sparse(13,13));

Hf{4}(8,4) = x(11);
Hf{4}(11,4) = x(8);
Hf{4}(4,8) = x(11);
Hf{4}(11,8) = x(4);
Hf{4}(4,11) = x(8);
Hf{4}(8,11) = x(4);


 Hf{5} = interval(sparse(13,13),sparse(13,13));

Hf{5}(2,1) = x(5);
Hf{5}(5,1) = x(2);
Hf{5}(1,2) = x(5);
Hf{5}(5,2) = x(1);
Hf{5}(1,5) = x(2);
Hf{5}(2,5) = x(1);


 Hf{6} = interval(sparse(13,13),sparse(13,13));

Hf{6}(4,2) = x(11);
Hf{6}(11,2) = x(4);
Hf{6}(2,4) = x(11);
Hf{6}(11,4) = x(2);
Hf{6}(2,11) = x(4);
Hf{6}(4,11) = x(2);


 Hf{7} = interval(sparse(13,13),sparse(13,13));

Hf{7}(7,1) = x(11);
Hf{7}(11,1) = x(7);
Hf{7}(1,7) = x(11);
Hf{7}(11,7) = x(1);
Hf{7}(1,11) = x(7);
Hf{7}(7,11) = x(1);


 Hf{8} = interval(sparse(13,13),sparse(13,13));

Hf{8}(1,1) = 6*x(1);


 Hf{9} = interval(sparse(13,13),sparse(13,13));

Hf{9}(6,1) = x(10);
Hf{9}(10,1) = x(6);
Hf{9}(1,6) = x(10);
Hf{9}(10,6) = x(1);
Hf{9}(1,10) = x(6);
Hf{9}(6,10) = x(1);


 Hf{10} = interval(sparse(13,13),sparse(13,13));

Hf{10}(3,2) = x(8);
Hf{10}(8,2) = x(3);
Hf{10}(2,3) = x(8);
Hf{10}(8,3) = x(2);
Hf{10}(2,8) = x(3);
Hf{10}(3,8) = x(2);


 Hf{11} = interval(sparse(13,13),sparse(13,13));

Hf{11}(6,6) = 6*x(6);


 Hf{12} = interval(sparse(13,13),sparse(13,13));

Hf{12}(9,1) = x(11);
Hf{12}(11,1) = x(9);
Hf{12}(1,9) = x(11);
Hf{12}(11,9) = x(1);
Hf{12}(1,11) = x(9);
Hf{12}(9,11) = x(1);
