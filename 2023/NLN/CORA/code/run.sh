#!/bin/bash
set -e

# install Multi Parametric Toolbox (required for CORA)
mkdir -p /toolboxes
cd /toolboxes
mkdir tbxmanager && cd tbxmanager
echo "
urlwrite('https://raw.githubusercontent.com/verivital/tbxmanager/master/tbxmanager.m', 'tbxmanager.m');
tbxmanager
tbxmanager install mpt mptdoc cddmex fourier glpkmex hysdel lcp sedumi espresso;
" > install_tbx.m
matlab -nodisplay -r "install_tbx; mpt_init; addpath(genpath('toolboxes')); savepath"

mkdir /results
cd /code

# reachable set computation with CORA
matlab -nodisplay -nosoftwareopengl -r \
  "run_arch23_nln_category()"

# collision checks
echo "Collision Checker (TRAF22) ------------------------------------------------"
echo " "

python3 check_collision.py /results/BEL_Putte-4_2_T-1_occupancies.csv