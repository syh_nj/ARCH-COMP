function [Tf,ind] = thirdOrderTensorInt_Robertson_case1_ARCH23(x,u)



 Tf{1,1} = interval(sparse(4,4),sparse(4,4));



 Tf{1,2} = interval(sparse(4,4),sparse(4,4));



 Tf{1,3} = interval(sparse(4,4),sparse(4,4));



 Tf{1,4} = interval(sparse(4,4),sparse(4,4));



 Tf{2,1} = interval(sparse(4,4),sparse(4,4));



 Tf{2,2} = interval(sparse(4,4),sparse(4,4));



 Tf{2,3} = interval(sparse(4,4),sparse(4,4));



 Tf{2,4} = interval(sparse(4,4),sparse(4,4));



 Tf{3,1} = interval(sparse(4,4),sparse(4,4));



 Tf{3,2} = interval(sparse(4,4),sparse(4,4));



 Tf{3,3} = interval(sparse(4,4),sparse(4,4));



 Tf{3,4} = interval(sparse(4,4),sparse(4,4));



 Tg{1,1} = interval(sparse(4,4),sparse(4,4));



 Tg{1,2} = interval(sparse(4,4),sparse(4,4));



 Tg{1,3} = interval(sparse(4,4),sparse(4,4));



 Tg{1,4} = interval(sparse(4,4),sparse(4,4));



 Tg{2,1} = interval(sparse(4,4),sparse(4,4));



 Tg{2,2} = interval(sparse(4,4),sparse(4,4));



 Tg{2,3} = interval(sparse(4,4),sparse(4,4));



 Tg{2,4} = interval(sparse(4,4),sparse(4,4));



 Tg{3,1} = interval(sparse(4,4),sparse(4,4));



 Tg{3,2} = interval(sparse(4,4),sparse(4,4));



 Tg{3,3} = interval(sparse(4,4),sparse(4,4));



 Tg{3,4} = interval(sparse(4,4),sparse(4,4));


 ind = cell(3,1);
 ind{1} = [];


 ind{2} = [];


 ind{3} = [];

