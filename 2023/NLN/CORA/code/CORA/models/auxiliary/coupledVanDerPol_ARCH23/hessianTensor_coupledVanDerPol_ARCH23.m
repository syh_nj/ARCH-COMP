function Hf=hessianTensor_coupledVanDerPol_ARCH23(x,u)



 Hf{1} = sparse(6,6);



 Hf{2} = sparse(6,6);

Hf{2}(1,1) = -2*x(2);
Hf{2}(2,1) = -2*x(1);
Hf{2}(5,1) = -1;
Hf{2}(1,2) = -2*x(1);
Hf{2}(5,3) = 1;
Hf{2}(1,5) = -1;
Hf{2}(3,5) = 1;


 Hf{3} = sparse(6,6);



 Hf{4} = sparse(6,6);

Hf{4}(5,1) = 1;
Hf{4}(3,3) = -2*x(4);
Hf{4}(4,3) = -2*x(3);
Hf{4}(5,3) = -1;
Hf{4}(3,4) = -2*x(3);
Hf{4}(1,5) = 1;
Hf{4}(3,5) = -1;


 Hf{5} = sparse(6,6);


end