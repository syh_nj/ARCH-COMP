function [Tf,ind] = thirdOrderTensorInt_coupledVanDerPol_ARCH23(x,u)



 Tf{1,1} = interval(sparse(6,6),sparse(6,6));



 Tf{1,2} = interval(sparse(6,6),sparse(6,6));



 Tf{1,3} = interval(sparse(6,6),sparse(6,6));



 Tf{1,4} = interval(sparse(6,6),sparse(6,6));



 Tf{1,5} = interval(sparse(6,6),sparse(6,6));



 Tf{1,6} = interval(sparse(6,6),sparse(6,6));



 Tf{2,1} = interval(sparse(6,6),sparse(6,6));

Tf{2,1}(2,1) = -2;
Tf{2,1}(1,2) = -2;


 Tf{2,2} = interval(sparse(6,6),sparse(6,6));

Tf{2,2}(1,1) = -2;


 Tf{2,3} = interval(sparse(6,6),sparse(6,6));



 Tf{2,4} = interval(sparse(6,6),sparse(6,6));



 Tf{2,5} = interval(sparse(6,6),sparse(6,6));



 Tf{2,6} = interval(sparse(6,6),sparse(6,6));



 Tf{3,1} = interval(sparse(6,6),sparse(6,6));



 Tf{3,2} = interval(sparse(6,6),sparse(6,6));



 Tf{3,3} = interval(sparse(6,6),sparse(6,6));



 Tf{3,4} = interval(sparse(6,6),sparse(6,6));



 Tf{3,5} = interval(sparse(6,6),sparse(6,6));



 Tf{3,6} = interval(sparse(6,6),sparse(6,6));



 Tf{4,1} = interval(sparse(6,6),sparse(6,6));



 Tf{4,2} = interval(sparse(6,6),sparse(6,6));



 Tf{4,3} = interval(sparse(6,6),sparse(6,6));

Tf{4,3}(4,3) = -2;
Tf{4,3}(3,4) = -2;


 Tf{4,4} = interval(sparse(6,6),sparse(6,6));

Tf{4,4}(3,3) = -2;


 Tf{4,5} = interval(sparse(6,6),sparse(6,6));



 Tf{4,6} = interval(sparse(6,6),sparse(6,6));



 Tf{5,1} = interval(sparse(6,6),sparse(6,6));



 Tf{5,2} = interval(sparse(6,6),sparse(6,6));



 Tf{5,3} = interval(sparse(6,6),sparse(6,6));



 Tf{5,4} = interval(sparse(6,6),sparse(6,6));



 Tf{5,5} = interval(sparse(6,6),sparse(6,6));



 Tf{5,6} = interval(sparse(6,6),sparse(6,6));



 Tg{1,1} = interval(sparse(6,6),sparse(6,6));



 Tg{1,2} = interval(sparse(6,6),sparse(6,6));



 Tg{1,3} = interval(sparse(6,6),sparse(6,6));



 Tg{1,4} = interval(sparse(6,6),sparse(6,6));



 Tg{1,5} = interval(sparse(6,6),sparse(6,6));



 Tg{1,6} = interval(sparse(6,6),sparse(6,6));



 Tg{2,1} = interval(sparse(6,6),sparse(6,6));



 Tg{2,2} = interval(sparse(6,6),sparse(6,6));



 Tg{2,3} = interval(sparse(6,6),sparse(6,6));



 Tg{2,4} = interval(sparse(6,6),sparse(6,6));



 Tg{2,5} = interval(sparse(6,6),sparse(6,6));



 Tg{2,6} = interval(sparse(6,6),sparse(6,6));



 Tg{3,1} = interval(sparse(6,6),sparse(6,6));



 Tg{3,2} = interval(sparse(6,6),sparse(6,6));



 Tg{3,3} = interval(sparse(6,6),sparse(6,6));



 Tg{3,4} = interval(sparse(6,6),sparse(6,6));



 Tg{3,5} = interval(sparse(6,6),sparse(6,6));



 Tg{3,6} = interval(sparse(6,6),sparse(6,6));



 Tg{4,1} = interval(sparse(6,6),sparse(6,6));



 Tg{4,2} = interval(sparse(6,6),sparse(6,6));



 Tg{4,3} = interval(sparse(6,6),sparse(6,6));



 Tg{4,4} = interval(sparse(6,6),sparse(6,6));



 Tg{4,5} = interval(sparse(6,6),sparse(6,6));



 Tg{4,6} = interval(sparse(6,6),sparse(6,6));



 Tg{5,1} = interval(sparse(6,6),sparse(6,6));



 Tg{5,2} = interval(sparse(6,6),sparse(6,6));



 Tg{5,3} = interval(sparse(6,6),sparse(6,6));



 Tg{5,4} = interval(sparse(6,6),sparse(6,6));



 Tg{5,5} = interval(sparse(6,6),sparse(6,6));



 Tg{5,6} = interval(sparse(6,6),sparse(6,6));


 ind = cell(5,1);
 ind{1} = [];


 ind{2} = [1;2];


 ind{3} = [];


 ind{4} = [3;4];


 ind{5} = [];

