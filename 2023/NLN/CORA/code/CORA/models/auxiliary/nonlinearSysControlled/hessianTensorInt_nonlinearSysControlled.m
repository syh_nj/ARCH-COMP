function Hf=hessianTensorInt_nonlinearSysControlled(x,u)



 Hf{1} = interval(sparse(7,7),sparse(7,7));

Hf{1}(3,3) = -x(4)*cos(x(3));
Hf{1}(4,3) = -sin(x(3));
Hf{1}(3,4) = -sin(x(3));


 Hf{2} = interval(sparse(7,7),sparse(7,7));

Hf{2}(3,3) = -x(4)*sin(x(3));
Hf{2}(4,3) = cos(x(3));
Hf{2}(3,4) = cos(x(3));


 Hf{3} = interval(sparse(7,7),sparse(7,7));



 Hf{4} = interval(sparse(7,7),sparse(7,7));



 Hf{5} = interval(sparse(7,7),sparse(7,7));



 Hf{6} = interval(sparse(7,7),sparse(7,7));


end