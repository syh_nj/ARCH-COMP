#!/bin/bash

docker build . -t "dummy-submit" --no-cache
docker run --mac-address 12:34:56:78:9a:bc --name "dummy-submit-container" "dummy-submit" /bin/bash -c "matlab -nodisplay -r "submit""
docker cp "dummy-submit-container":/results/ .
docker rm --force "dummy-submit-container"
docker image rm --force "dummy-submit"

