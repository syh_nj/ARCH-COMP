function res = test_nn_nnHelper_calcSquared()
% test_nn_nnHelper_calcSquared - tests the nnHelper.calcSquared function
%
% Syntax:  
%    res = test_nn_nnHelper_calcSquared()
%
% Inputs:
%    -
%
% Outputs:
%    res - boolean 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: nnHelper/calcSquared, nnHelper/calcSquaredE

% Author:       Tobias Ladner
% Written:      17-February-2023
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% init
c = [1];
G = [2 1.5 1];
Grest = []; % only using Grest=[] to avoid over-approximation
expMat = [1 0 3;0 1 1];

% compute pZ^2
[c2, G2, Grest2] = nnHelper.calcSquared( ...
    c, G, Grest, expMat, ...
    c, G, Grest, expMat, true);
expMat2 = nnHelper.calcSquaredE(expMat, expMat, true);
expMat2 = [expMat, expMat, expMat2];
pZ2 = polyZonotope(c2, G2, Grest2, expMat2);

% compute pZ^3
[c3, G3, Grest3] = nnHelper.calcSquared( ...
    c2, G2, Grest2, expMat2, ...
    c, G, Grest, expMat, false);
expMat3 = nnHelper.calcSquaredE(expMat2, expMat, false);
expMat3 = [expMat2, expMat, expMat3];
pZ3 = polyZonotope(c3, G3, Grest3, expMat3);

% compute reference
pZ_ref = polyZonotope(c,G,Grest,expMat);
pZ2_ref = quadMap(pZ_ref, {1});
pZ3_ref = cubMap(pZ_ref, {1});

% check equality
res = isequal(pZ2, pZ2_ref);
res = res && isequal(pZ3, pZ3_ref);

end

%------------- END OF CODE --------------