function obj = readONNXNetwork(file_path, varargin)
% readONNXNetwork - reads and converts a network saved in onnx format
%    Note: If the onnx network contains a custom layer, this function will
%    create a +CustomLayer package folder containing all custom layers in
%    your current MATLAB directory.
%
% Syntax:
%    res = neuralNetwork.readONNXNetwork(file_path)
%
% Inputs:
%    file_path - path to file
%    verbose - bool if information should be displayed
%    inputDataFormats - dimensions of input e.g. 'BC' or 'BSSC'
%    outputDataFormats - see inputDataFormats
%
% Outputs:
%    obj - generated object
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Tobias Ladner
% Written:      30-March-2022
% Last update:  07-June-2022 (specify in- & outputDataFormats)
%               30-November-2022 (removed neuralNetworkOld)
%               13-February-2023 (simplified function)
% Last revision:---

%------------- BEGIN CODE --------------

% validate parameters
if nargin < 1
    throw(CORAerror("CORA:notEnoughInputArgs", 1))
elseif nargin > 5
    throw(CORAerror("CORA:tooManyInputArgs", 5))
end
[verbose, inputDataFormats, outputDataFormats, targetNetwork] = ...
    setDefaultValues({false, 'BC', 'BC', 'dagnetwork'}, varargin);

% valid in-/outputDataFormats for importONNXNetwork
validDataFormats = {'BC','BCSS','BSSC','CSS','SSC','BCSSS','BSSSC', ...
    'CSSS','SSSC','TBC','BCT','BTC','1BC','T1BC','TBCSS','TBCSSS'};
inputArgsCheck({ ...
    {verbose, 'att', 'logical'}; ...
    {inputDataFormats, 'str', validDataFormats}; ...
    {outputDataFormats, 'str', validDataFormats}; ...
    {targetNetwork, 'str', {'dagnetwork', 'dlnetwork'}}; ...
})


% dlnetwork
customLayerName = 'CustomLayers';

if verbose
    disp("Reading network...")
end

dltoolbox_net = importONNXNetwork(file_path, ...
    'InputDataFormats', inputDataFormats, ...
    'OutputDataFormats', outputDataFormats, ...
    'PackageName', customLayerName, ...
    'TargetNetwork', targetNetwork);
obj = neuralNetwork.convertDLToolboxNetwork(dltoolbox_net.Layers, verbose);

end

% Auxiliary functions -----------------------------------------------------

%------------- END OF CODE --------------