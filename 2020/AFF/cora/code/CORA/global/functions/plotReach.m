function handle = plotReach(R,varargin)
% plotReach - plot the whole reachable set of the time intervals
%
% Syntax:  
%    handle = plotReach(R)
%    handle = plotReach(R,dim,color,type,order)
%
% Inputs:
%    R - cell-array containing the sets which describe the reachable set
%    dim - two-dimensional vector containing the displayed dimensions
%    color - color for the plot (i.e. 'r', 'g', or [0.5 0.5 0.5])
%    type - plot type ('filled' or 'contour')
%    order - zonotope order for the plots
%
% Outputs:
%    handle - handle to the resulting graphics object
%
% Example: 
%    ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: plotReachDiscrete

% Author:       Niklas Kochdumper
% Written:      13-September-2018
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% default parameter
dim = [1,2];
color = 'b';
type = 'filled';
order = [];

% parse input arguments
if nargin >= 2 && ~isempty(varargin{1})
    dim = varargin{1};
end
if nargin >= 3 && ~isempty(varargin{2})
    color = varargin{2};
end
if nargin >= 4 && ~isempty(varargin{3})
    type = varargin{3};
end
if nargin >= 5 && ~isempty(varargin{4})
    order = varargin{4};
end

% suppress all warnings
warnOld = warning;
warning('off','all');

% different modes for plotting
if strcmp(type,'filled')

    poly = [];
    
    % loop over all sets
    for i = 1:length(R)

        if iscell(R{i})
            
            % loop over all parallel sets
            for j = 1:length(R{i})
                
               % project set to desired dimension
               set = R{i}{j};
               temp = project(set,dim);
               
               % reduce
               if ~isempty(order)
                  temp = reduce(temp,'girard',order); 
               end
               
               % calculate vertices for the plot
               vert = vertices(temp);
               
               % add set to overall polgon
               if isempty(poly)
                  poly = polyshape(vert(1,:),vert(2,:)); 
               else
                  poly = union(poly,polyshape(vert(1,:),vert(2,:)));
               end
            end
            
        else
            % project set to desired dimension
            set = R{i};
            temp = project(set,dim);
            
            % reduce 
            if ~isempty(order)
              temp = reduce(temp,'girard',order); 
            end
            
            % calculate vertices for the plot
            vert = vertices(temp);
            
            % add set to overall polgon
            if isempty(poly)
              poly = polyshape(vert(1,:),vert(2,:)); 
            else
              poly = union(poly,polyshape(vert(1,:),vert(2,:)));
            end
        end
    end
    
    % plot the polygon
    handle = plot(poly,'FaceAlpha',1,'FaceColor',color,'EdgeColor','none');
    
    
else
    
    % loop over all sets
    for i = 1:length(R)

        if iscell(R{i})
            
            % loop over all parallel sets
            for j = 1:length(R{i})
                
               % project set to desired dimension
               set = R{i}{j};
               temp = project(set,dim);
               
               % reduce
               if ~isempty(order)
                  temp = reduce(temp,'girard',order); 
               end
               
               % plot 
               handle = plot(temp,[1,2],color);
            end
            
        else
            % project set to desired dimension
            set = R{i};
            temp = project(set,dim);
            
            % reduce 
            if ~isempty(order)
              temp = reduce(temp,'girard',order); 
            end
            
            % plot 
            handle = plot(temp,[1,2],color);
        end
    end    
end

% restore original warning settings
warning(warnOld);

%------------- END OF CODE --------------