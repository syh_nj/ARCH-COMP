#include "ibex.h"

using namespace ibex;
#include "vibes.cpp"

unsigned long int i = 0;
void toGunPlot (const simulation* sim) {

  std::list<solution_g>::const_iterator iterator_list;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
    i++;
    std::cout << "set object " << i <<  " rect from " <<
      iterator_list->time_j.lb() << ", " <<
      iterator_list->box_j1->operator[](2).lb() << " to " <<
      iterator_list->time_j.ub() << ", " <<
      iterator_list->box_j1->operator[](2).ub() <<
      std::endl;
  }

}


void plot_simu(const simulation* sim, string color)
{
  std::list<solution_g>::const_iterator iterator_list;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
  	if (iterator_list->time_j.lb() > 50)
  		return;
    vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   iterator_list->box_j1->operator[](2).lb(), iterator_list->box_j1->operator[](2).ub(), color);

  }
}


int main(int argc, char *argv[]){
  const int ny= 12;
  Variable y(ny);
  IntervalVector yinit(ny);
  double delta=atoi(argv[1])/10.0;//0.1, 0.4, 0.8
  
  yinit[0] = Interval(-delta,delta);
  yinit[1] = Interval(-delta,delta);
  yinit[2] = Interval(-delta,delta);
  yinit[3] = Interval(-delta,delta);
  yinit[4] = Interval(-delta,delta);
  yinit[5] = Interval(-delta,delta);
  yinit[6] = Interval(0);
  yinit[7] = Interval(0);
  yinit[8] = Interval(0);
  yinit[9] = Interval(0);
  yinit[10] = Interval(0);
  yinit[11] = Interval(0);

  double g=9.81;
  double R=0.1;
  double l=0.5;
  double Mr=0.1;
  double M=1;
  double m=M+4*Mr;
  double Jx=(2.0/5.0)*M*(R*R)+2*(l*l)*Mr;
  double Jy=Jx;
  double Jz=(2.0/5.0)*M*(R*R)+4*(l*l)*Mr;

  //consignes
  double u1=1;
  double u2=0;
  double u3=0;

  Array<const ExprNode> a(ny);
  a.set_ref(0,cos(y[7])*cos(y[8])*y[3]+(sin(y[6])*sin(y[7])*cos(y[8]) - cos(y[6])*sin(y[8]))*y[4] + (cos(y[6])*sin(y[7])*cos(y[8]) + sin(y[6])*sin(y[8]))*y[5]);
  a.set_ref(1,cos(y[7])*sin(y[8])*y[3]+(sin(y[6])*sin(y[7])*sin(y[8]) + cos(y[6])*cos(y[8]))*y[4] + (cos(y[6])*sin(y[7])*sin(y[8]) - sin(y[6])*cos(y[8]))*y[5]);
  a.set_ref(2,sin(y[7])*y[3]-sin(y[6])*cos(y[7])*y[4]-cos(y[6])*cos(y[7])*y[5]);
  a.set_ref(3,y[11]*y[4]-y[10]*y[5]-g*sin(y[7]));
  a.set_ref(4,y[9]*y[5]-y[11]*y[3]+g*cos(y[7])*sin(y[6]));
  a.set_ref(5,y[10]*y[3]-y[9]*y[4]+g*cos(y[7])*cos(y[6])-(m*g-10*(y[2]-u1)+3*y[5])/m);
  a.set_ref(6,y[9]+sin(y[6])*(sin(y[7])/cos(y[7]))*y[10]+cos(y[6])*(sin(y[7])/cos(y[7]))*y[11]);
  a.set_ref(7,cos(y[6])*y[10]-sin(y[6])*y[11]);
  a.set_ref(8,(sin(y[6])/cos(y[7]))*y[10]+(cos(y[6])/cos(y[7]))*y[11]);
  a.set_ref(9,((Jy-Jz)/Jx)*y[10]*y[11]+(1/Jx)*(-(y[6]-u2)-y[9]));
  a.set_ref(10,((Jz-Jx)/Jy)*y[9]*y[11]+(1/Jy)*(-(y[7]-u3)-y[10]));
  a.set_ref(11,((Jx-Jy)/Jz)*y[9]*y[10]);
  const ExprVector& _return = ExprVector::new_(a, true);
  Function ydot = Function(y, _return);


  ivp_ode problem = ivp_ode(ydot,0.0,yinit);

  simulation simu = simulation(&problem,5.0,HEUN,1e-8);


  AF_fAFFullI::setAffineNoiseNumber (60);
  simu.run_simulation();

  IntervalVector safebox(ny, Interval::ALL_REALS);
  safebox[2] = Interval(NEG_INFINITY, 1.4);

  IntervalVector goalbox(ny, Interval::ALL_REALS);
  goalbox[2] = Interval(0.98, 1.02);

  if (simu.stayed_in(safebox)) {
    if (simu.finished_in(goalbox)) {
      //std::cerr << "goal sucess" << std::endl;
    }
    else {
      std::cerr << "goal failure" << std::endl;
    }
  }
  else {
    std::cerr << "safety failure" << std::endl;
  }

 /* std::cout << "set terminal png" << std::endl;
  std::cout << "set output 'quadcopter.png'" << std::endl;
  std::cout << "set xrange [0:5]" << std::endl;
  std::cout << "set yrange [-0.5:1.5]" << std::endl;
  std::cout << "set nokey" << std::endl;
  std::cout << "set style rectangle back fc rgb 'blue' fs solid 1.0 noborder" << std::endl;

  toGunPlot (&simu);

  std::cout << "plot 1.4 lt rgb 'red', 1.02 lt rgb 'red', 0.8 lt rgb 'red'" << std::endl;
*/

    std::cout << "Final volume : " << (simu.get_last()).volume() << std::endl;
  
  vibes::beginDrawing ();
  vibes::selectFigure("Quad");


  string color;
  if (delta==0.1)
  	color="red[red]";
  if (delta==0.4)
  	color="green[green]";
  if (delta==0.8)
  	color="blue[blue]";
  plot_simu(&simu,color);
  
   vibes::axisAuto();
  vibes::setFigureProperty("Quad","width",800);
  vibes::setFigureProperty("Quad","height",600);
  string filename = "Quad"+string(argv[1])+".jpg";
  vibes::saveImage(filename,"Quad");
  
  if (delta==0.1)
  	vibes::closeFigure("Quad");
  vibes::endDrawing();

  return 0;

}
