function [Zred]=reduceAlthoff(Z)
% reduceGirard - Reduce zonotope so that its order is one
%
% Syntax:  
%    [Zred]=reduceAlthoff(Z)
%
% Inputs:
%    Z - zonotope object
%
% Outputs:
%    Zred - reduced zonotope
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: ---

% Author: Matthias Althoff
% Written: 14-September-2007 
% Last update: 22-March-2007
%              27-Aug-2019
% Last revision: ---

%------------- BEGIN CODE --------------


%initialize Z_red
Zred=Z;

%extract generator matrix
G=generators(Z);

%determine dimension of zonotope
[dim, ~] = size(G);

%Delete zero-generators
G = nonzeroFilter(G);

%determine first generator
for i=1:length(G(1,:))
    h(i)=norm(G(:,i)'*G,1);
end
[value,index]=max(h);
Gpicked(:,1)=G(:,index);

%remove picked generator
G(:,index)=[];

%pick further generators
for i=1:(dim-1)
    h=[];
    for j=1:length(G(1,:))
        h(j)=norm(G(:,j)'*Gpicked,1)/norm(G(:,j))^1.2;
    end
    [value,index]=min(h);
    %pick generator
    Gpicked(:,end+1)=G(:,index);
    %remove picked generator
    G(:,index)=[];    
end

%Build transformation matrix P
for i=1:dim
    P(:,i)=Gpicked(:,i)/norm(Gpicked(:,i));
end

%Project Zonotope into new coordinate system
Ztrans=inv(P)*Z;
Zinterval=interval(Ztrans);
Zred=P*zonotope(Zinterval);

%------------- END OF CODE --------------
