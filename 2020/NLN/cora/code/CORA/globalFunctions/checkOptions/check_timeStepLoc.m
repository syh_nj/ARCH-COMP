function check_timeStepLoc(options, obj)
% check_timeStepLoc - checks if options.timeStepLoc
%  1) exists
%  2) takes an allowed value
%
% Syntax:
%    check_y0guess(options, obj)
%
% Inputs:
%    options - options for object
%    obj     - system object
%
% Outputs:
%    -
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none
%
% References: 
%   -

% Author:       Niklas Kochdumper, Mark Wetzlinger
% Written:      08-May-2020
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

option = 'timeStepLoc';
strct = 'options';

locations = get(obj, 'location');
numLoc = length(locations);
% timeStepLoc is a cell of length #locations, each entry > 0
if ~isfield(options,option)
    error(printOptionMissing(obj,option,strct));
elseif length(options.timeStepLoc) ~= numLoc
    error(printOptionOutOfRange(obj,option,strct));
else
    for i=1:numLoc
        if options.timeStepLoc{i} < 0
            error(printOptionOutOfRange(obj,option,strct));
        end
    end
end


end

%------------- END OF CODE --------------