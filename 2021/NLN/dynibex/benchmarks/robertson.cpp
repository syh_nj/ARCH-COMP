/* ============================================================================
 * Robertson's problem
 *
 * ---------------------------------------------------------------------------- */

#include "ibex.h"
#include "vibes.cpp"
#include "vibes.h"
#include <chrono>
using namespace std::chrono;

using namespace ibex;

void plot_simu_blue(const simulation* sim)
{
  std::list<solution_g>::const_iterator iterator_list;
  Interval sum;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
  	if (iterator_list->time_j.lb() > 50)
  		return;
   sum = iterator_list->box_j1->operator[](0)+iterator_list->box_j1->operator[](1)+iterator_list->box_j1->operator[](2);
    vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   sum.lb(), sum.ub(), "blue[blue]");
	
    /*vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   iterator_list->box_j1->operator[](1).lb(), iterator_list->box_j1->operator[](1).ub(), "blue[blue]");

    vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   iterator_list->box_j1->operator[](2).lb(), iterator_list->box_j1->operator[](2).ub(), "black[black]");*/
  }
  cout << "diam " << sum.diam() << endl;
}

void plot_simu_green(const simulation* sim)
{
  std::list<solution_g>::const_iterator iterator_list;
  Interval sum;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
  	if (iterator_list->time_j.lb() > 50)
  		return;
  	sum = iterator_list->box_j1->operator[](0)+iterator_list->box_j1->operator[](1)+iterator_list->box_j1->operator[](2);
    vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   sum.lb(), sum.ub(), "green[green]");

    /*vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   iterator_list->box_j1->operator[](1).lb(), iterator_list->box_j1->operator[](1).ub(), "blue[blue]");

    vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   iterator_list->box_j1->operator[](2).lb(), iterator_list->box_j1->operator[](2).ub(), "black[black]");*/
  }
  cout << "diam " << sum.diam() << endl;
}

void plot_simu_red(const simulation* sim)
{
  std::list<solution_g>::const_iterator iterator_list;
  Interval sum;
  for(iterator_list=sim->list_solution_g.begin();iterator_list!=sim->list_solution_g.end();iterator_list++) {
  	if (iterator_list->time_j.lb() > 50)
  		return;
  	sum = iterator_list->box_j1->operator[](0)+iterator_list->box_j1->operator[](1)+iterator_list->box_j1->operator[](2);
    vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   sum.lb(), sum.ub(), "red[red]");

    /*vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   iterator_list->box_j1->operator[](1).lb(), iterator_list->box_j1->operator[](1).ub(), "blue[blue]");

    vibes::drawBox(iterator_list->time_j.lb(), iterator_list->time_j.ub(),
		   iterator_list->box_j1->operator[](2).lb(), iterator_list->box_j1->operator[](2).ub(), "black[black]");*/
  }
    cout << "diam " << sum.diam() << endl;
}

int main(){

const int n= 3;
Variable y(n);
IntervalVector yinit(n);
yinit[0] = Interval(1.0);
yinit[1] = Interval(0.0);
yinit[2] = Interval(0.0);

double alpha=0.4;  //0.4 ;rob 0.4
double beta=100;   //100, 1000 1000;rob 1000
double gamma=1000; //1000 100000 10000000;rob 10000000

Function ydot = Function(y,Return( -alpha*y[0] + beta*y[1]*y[2],
alpha*y[0]-beta*y[1]*y[2]-gamma*y[1]*y[1],
gamma*y[1]*y[1]));

NumConstraint csp1(y,y[0]+y[1]+y[2] - 1.0 = 0);
  NumConstraint csp2(y,y[0]>=0);
  NumConstraint csp3(y,y[1]>=0);
  NumConstraint csp4(y,y[2]>=0);
  NumConstraint csp5(y,y[0]<=1);
  NumConstraint csp6(y,y[1]<=1);
  NumConstraint csp7(y,y[2]<=1);
  

  Array<NumConstraint> csp(csp1,csp2,csp3,csp4,csp5,csp6,csp7);


ivp_ode problem = ivp_ode(ydot,0.0,yinit,csp,SYMBOLIC);

simulation simu = simulation(&problem,40.0,LC3,1e-11, 1.e-2); //1
/*[0.00222015, 0.00222015]
real	19m23,362s
user	19m21,187s
sys	0m0,144s
Solution at t=40.000000 : 
([0.312306, 0.314134] ; [0.00177694, 0.00178904] ; [0.684807, 0.685187])
Diameter : (0.00182751 ; 1.21047e-05 ; 0.000380529)
Rejected picard :5
Accepted picard :8694
Step min :0.00125
Step max :0.00579645
Truncature error max :1.57556e-11
*/

alpha=0.4;  //0.4 ;rob 0.4
beta=1000;   //100, 1000 1000;rob 1000
gamma=100000; //1000 100000 10000000;rob 10000000

Function ydot2 = Function(y,Return( -alpha*y[0] + beta*y[1]*y[2],
alpha*y[0]-beta*y[1]*y[2]-gamma*y[1]*y[1],
gamma*y[1]*y[1]));


ivp_ode problem2 = ivp_ode(ydot2,0.0,yinit,csp,SYMBOLIC);

simulation simu2 = simulation(&problem2,40.0,LC3,1e-12, 1.e-3); //2
/*
[0.00408499, 0.00408499]
real	222m31,541s
user	221m48,934s
sys	0m1,780s
Solution at t=40.000000 : 
([0.311781, 0.315144] ; [0.000176962, 0.00017917] ; [0.685999, 0.686719])
Diameter : (0.0033631 ; 2.20803e-06 ; 0.000719685)
Rejected picard :2
Accepted picard :84460
Step min :8.62766e-05
Step max :0.000802707
Truncature error max :1.58563e-12
*/

alpha=0.4;  //0.4 ;rob 0.4
beta=1000;   //100, 1000 1000;rob 1000
gamma=10000000; //1000 100000 10000000;rob 10000000

Function ydot3 = Function(y,Return( -alpha*y[0] + beta*y[1]*y[2],
alpha*y[0]-beta*y[1]*y[2]-gamma*y[1]*y[1],
gamma*y[1]*y[1]));


ivp_ode problem3 = ivp_ode(ydot3,0.0,yinit,csp,SYMBOLIC);

simulation simu3 = simulation(&problem3,40.0,LC3,1e-12, 1.e-4); //3
/*
[0.000785901, 0.000785901]
real	364m7,536s
user	343m27,308s
sys	0m14,041s
Solution at t=40.000000 : 
([0.021, 0.0214082] ; [7.92187e-06, 8.09846e-06] ; [0.978599, 0.978977])
Diameter : (0.00040825 ; 1.76588e-07 ; 0.000377474)
Rejected picard :2
Accepted picard :123248
Step min :2.5e-05
Step max :0.000445995
Truncature error max :1.9895e-12
*/

auto start = high_resolution_clock::now();

simu.run_simulation();

auto stop = high_resolution_clock::now();
auto duration = duration_cast<seconds>(stop - start);
cout << "Time taken by function: " << duration.count() << " seconds" << endl;

  Affine2Vector last=simu.get_last_aff();
  Interval last_width = ((last[0]+last[1]+last[2]).itv()).diam();
  
  IntervalVector lasti=simu.get_last();
  Interval last_widthi = ((lasti[0]+lasti[1]+lasti[2])).diam();
  
  cout << last_width << endl;
  cout << last_widthi << endl;


start = high_resolution_clock::now();
simu2.run_simulation();
stop = high_resolution_clock::now();
duration = duration_cast<seconds>(stop - start);
cout << "Time taken by function: " << duration.count() << " seconds" << endl;

  last=simu2.get_last_aff();
  last_width = ((last[0]+last[1]+last[2]).itv()).diam();
  
  cout << last_width << endl;
  
start = high_resolution_clock::now();
simu3.run_simulation();
stop = high_resolution_clock::now();
duration = duration_cast<seconds>(stop - start);
cout << "Time taken by function: " << duration.count() << " seconds" << endl;

  last=simu3.get_last_aff();
  last_width = ((last[0]+last[1]+last[2]).itv()).diam();
  
  cout << last_width << endl;

  vibes::beginDrawing ();
  vibes::newFigure("Rob");
  vibes::axisLimits(0,40,0.999,1.001);
  //vibes::setFigureProperty("plot","time",40);
  //vibes::setFigureProperty("plot","y",2);
  vibes::setFigureProperty("Rob","width",800);
  vibes::setFigureProperty("Rob","height",600);

  plot_simu_blue(&simu);
  plot_simu_green(&simu2);
  plot_simu_red(&simu3);

  vibes::saveImage("robertson.jpg","Rob");
  
  vibes::closeFigure("Rob");

  vibes::endDrawing();
  

  //[0.999601, 1.0004]

//For an export in order to plot
//simu.generate_python_plot("rob.py", 0, 1);

return 0;

}
