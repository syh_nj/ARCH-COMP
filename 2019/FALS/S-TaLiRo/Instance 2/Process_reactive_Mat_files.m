%load the .mat results file to be processed and then run the script
%NOTE Run THIS Script for "reactive" .mat result files
    % if file name does not include "reactive" then run other process script

nT_SOAR=[]; %store number of simulations of falsifying runs
nR_SOAR=[]; %store lowest robustness of non-falsifying runs
numOfFals_SOAR=0; 
Fals_Inps_SOAR=[]; %store the falsifying input of each falsifying run
outer_runs_SOAR = length(results_suff);

for iii=1:outer_runs_SOAR
    if results_suff(iii).run.falsified==1
        Fals_Inps_SOAR = [Fals_Inps_SOAR; results_suff(iii).run.bestSample'];
        nT_SOAR=[nT_SOAR, results_suff(iii).run.nTests];
        numOfFals_SOAR=numOfFals_SOAR+1;
    else
        nR_SOAR=[nR_SOAR; results_suff(iii).run.bestRob];
    end
end

disp(['number of falsifications: ',num2str(numOfFals_SOAR),'/50'])
disp(['Average number of runs to falsify: ', num2str(mean(nT_SOAR))])
disp(['Median number of runs to falsify: ', num2str(median(nT_SOAR))])