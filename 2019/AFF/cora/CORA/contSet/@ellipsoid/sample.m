function [Y] = sample(E,N)
% sample - returns N samples that are contained within E
%
% Syntax:  
%    Y = sample(E,N) gives an array of samples which are contained in E
% Inputs:
%    E - ellipsoids object
%    N - #Samples
%
% Outputs:
%    Y - Array of sampled points
%
% Example: 
%    E = ellipsoid.generate();
%    Y = sample(E,1000);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      27-July-2016
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------
b = boundary(ellipsoid(E.Q),N);

if ~E.isdegenerate && E.dim>1
    Y= rand(1,size(b,2)).*b+repmat(E.q,1,N);
else
    Y = b+repmat(E.q,1,N);
end
%------------- END OF CODE --------------