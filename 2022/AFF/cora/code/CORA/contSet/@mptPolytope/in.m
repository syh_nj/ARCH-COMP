function res = in(P,S,varargin)
% in - determines if a polytope contains another set or a point
%
% Syntax:  
%    res = in(P,S)
%    res = in(P,S,tol)
%
% Inputs:
%    P - mptPolytope object
%    S - contSet object or single point
%    tol - numerical tolerance for point in set containment
%
% Outputs:
%    res - true/false
%
% Example: 
%    P1 = mptPolytope([-1 -1; 1 0;-1 0; 0 1; 0 -1],[2;3;2;3;2]);
%    P2 = mptPolytope([-1 -1; -1 1; 1 1;0 -1],[0;2;2;0]);
%    P3 = poly2 + [2;0];
%
%    in(poly1,poly2)
%    in(poly1,poly3)
%
%    figure; hold on;
%    plot(P1,[1,2],'b');
%    plot(P2,[1,2],'g');
%
%    figure; hold on;
%    plot(P1,[1,2],'b');
%    plot(P3,[1,2],'r');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/in

% Author:       Niklas Kochdumper
% Written:      19-November-2019
% Last update:  26-July-2021 (VG: extended to multiple points)
% Last revision:---

%------------- BEGIN CODE --------------

% default input arguments
tol = setDefaultValues({{eps}},varargin{:});

% check input arguments
inputArgsCheck({{P,'att',{'mptPolytope'},{''}};
                {S,'att',{'numeric','contSet'},{''}}; ...
                {tol,'att',{'numeric'},{'nonnan','nonnegative','scalar'}}});

% init result
res = false;

% get object properties
A = P.P.A;
b = P.P.b;

% point in polytope containment
if isnumeric(S)
    tmp = A*S - b;
    if all(all(tmp < tol | withinTol(tmp,tol)))
        res = true;
    end

% other set in polytope containment
else

    % loop over all halfspaces
    for i = 1:size(A,1)
        b_ = supportFunc(S,A(i,:)','upper');
        if b_ > b(i) && ~withinTol(b(i),b_,tol)
            return 
        end
    end
    
    res = true;

end

%------------- END OF CODE --------------
