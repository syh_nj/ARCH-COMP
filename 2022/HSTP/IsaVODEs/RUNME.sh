#!/bin/sh

docker build --progress=plain --tag isaarch:1.0 .
docker run -d --name isa_container isaarch:1.0
docker cp isa_container:/home/isabelle/document.pdf .
docker rm -f isa_container
