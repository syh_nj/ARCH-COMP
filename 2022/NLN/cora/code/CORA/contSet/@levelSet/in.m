function res = in(ls,S,varargin)
% in - determines if a level set contains a set or a point
%
% Syntax:  
%    res = in(ls,S)
%    res = in(ls,S,tol)
%
% Inputs:
%    ls - levelSet object
%    S - contSet object or single point
%    tol - numerical tolerance (only for point in set containment)
%
% Outputs:
%    res - true/false
%
% Example: 
%    syms x y
%    eq = sin(x) + y;
%    ls = levelSet(eq,[x;y],'<=');
%
%    I1 = interval([0.7;-0.3],[1.3;0.3]);
%    I2 = interval([-1.3;-0.3],[-0.7;0.3]);
%
%    in(ls,I1)
%    in(ls,I2)
%
%    figure; hold on; xlim([-1.5,1.5]); ylim([-1,1]);
%    plot(ls,[1,2],'b');
%    plot(I1,[1,2],'FaceColor','r');
%
%    figure; hold on; xlim([-1.5,1.5]); ylim([-1,1]);
%    plot(ls,[1,2],'b');
%    plot(I2,[1,2],'FaceColor','g');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: halfspace/in, conHyperplane/in

% Author:       Niklas Kochdumper
% Written:      19-July-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

% default input arguments
tol = setDefaultValues({{eps}},varargin{:});

% check input arguments
inputArgsCheck({{ls,'att',{'levelSet'},{''}};
                {S,'att',{'numeric','contSet'},{''}}; ...
                {tol,'att',{'numeric'},{'nonnan','nonnegative','scalar'}}});

% set or single point 
if ~isnumeric(S)                                     % set

    % check type of level set
    if strcmp(ls.compOp,'==')
        throw(CORAerror('CORA:noops',ls,S));
    end

    % interval over-approximation
    I = interval(S); 

    % evaluate non-linear function with interval arithmetic
    I = ls.funHan(I);

    ub = supremum(I);

    % multiple inequality constraints or not
    if ~iscell(ls.compOp)

        % switch case for the different types of level sets
        if strcmp(ls.compOp,'<=')
            res = ub < 0 | withinTol(ub,0);
        else
            res = ub < 0;
        end

    else

        resVec = false(length(ls.compOp),1);

        % loop over all inequality constraints
        for i = 1:length(ls.compOp)

            if strcmp(ls.compOp{i},'<=')
                resVec(i) = ub(i) < 0 | withinTol(ub(i),0);
            else
                resVec(i) = ub(i) < 0;
            end
        end

        res = all(resVec);
    end

else                                                    % single point
    
    % evaluate nonlinear function
    val = ls.funHan(S);
    
     % multiple inequality constraints or not
    if ~iscell(ls.compOp)

        % switch case for the different types of level sets
        if strcmp(ls.compOp,'==')
            tmp = abs(val);
            res = tmp < tol | withinTol(tmp,tol);
        elseif strcmp(ls.compOp,'<=')
            res = val < tol | withinTol(val,tol);
        else
            res = val < tol;
        end

    else

        resVec = false(length(ls.compOp),1);

        % loop over all inequality constraints
        for i = 1:length(ls.compOp)

            if strcmp(ls.compOp{i},'==')
                tmp = abs(val(i));
                resVec(i) = tmp < tol | withinTol(tmp,tol);
            elseif strcmp(ls.compOp{i},'<=')
                resVec(i) = val(i) < tol | withinTol(val(i),tol);
            else
                resVec(i) = (val(i) < tol);
            end
        end

        res = all(resVec);
    end
end

%------------- END OF CODE --------------