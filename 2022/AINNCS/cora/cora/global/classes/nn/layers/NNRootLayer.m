classdef NNRootLayer < NNActivationLayer
    % NNRootLayer - class for square root layers
    %
    % Syntax:
    %    obj = NNRootLayer(name)
    %
    % Inputs:
    %    name - name of the layer, defaults to type
    %
    % Outputs:
    %    obj - generated object
    %
    % References:
    %    -
    %
    % Other m-files required: none
    % Subfunctions: none
    % MAT-files required: none
    %
    % See also: NeuralNetwork
    %
    % Author:       Tobias Ladner
    % Written:      04-July-2022
    % Last update:  ---
    % Last revision:---

    %------------- BEGIN CODE --------------

    properties (Constant)
        type = "RootLayer"
    end

    methods
        % constructor
        function obj = NNRootLayer(name)
            if nargin < 2
                name = NNRootLayer.type;
            end
            % call super class constructor
            obj@NNActivationLayer(name)
        end

        function df_i = getDf(obj, i)
            if i == 0
                df_i = obj.f;
            else
                df_i1 = obj.getDf(i-1);
                df_i = @(x) 1 / 2 .* df_i1(x);
            end
        end

        function der1 = getDerBounds(obj, l, u)
            der1 = interval(obj.df(l), obj.df(u));
        end

        % evaluate
        function r = evaluateNumeric(obj, input)
            r = sqrt(input);
        end

        function [res, d] = evaluateZonotopeNeuron(obj, input)
            error("Not Implemented!")
        end

        function [c, G, Grest, d] = evaluatePolyZonotopeNeuronLin(obj, c, G, Grest, E, ind, ind_, approx)
            error("Not Implemented!")
        end

        function [c, G, Grest, d] = evaluatePolyZonotopeNeuronQuad(obj, c, G, Grest, E, ind, ind_, approx)
            error("Not Implemented!")
        end

        function [c, G, Grest, d] = evaluatePolyZonotopeNeuronCub(obj, c, G, Grest, E, ind, ind_, approx)
            error("Not Implemented!")
        end

        function r = evaluateTaylmNeuron(obj, input, evParams)
            error("Not Implemented!")
        end

        function [c, G, C, d, l_, u_] = evaluateConZonotopeNeuron(obj, c, G, C, d, l_, u_, j, options, evParams)
            error("Not Implemented!")
        end
    end
end